---
title: "Check the format of a WAV file using the command 'file'"
date: 2023-05-26T23:07:31+09:00
draft: false
toc: false
images:
tags:
  - Ubuntu20
  - file
  - WAV
  - Audio
categories:
  - Posts
  - Linux
  - Ubuntu
---

## file a WAV file
```
$ file 00a2faca-e1f2-4848-9afe-058f949d3252.wav
00a2faca-e1f2-4848-9afe-058f949d3252.wav: RIFF (little-endian) data, WAVE audio, Microsoft PCM, 16 bit, mono 48000 Hz
```

## WAV format reference
[https://infograph.tistory.com/333](https://infograph.tistory.com/333)
