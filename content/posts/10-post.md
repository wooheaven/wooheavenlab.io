---
title: "Install Vim plugin vim-airline using vim-plug"
date: 2023-05-23T21:31:55+09:00
draft: false
toc: false
images:
tags:
  - Ubuntu20
  - vim
  - vim_plugin
  - vim-plug
  - vim-airline
  - .vimrc
categories:
  - Posts
  - Linux
  - Ubuntu
---

## Configure ~/.vimrc
```
$ vi ~/.vimrc
call plug#begin('~/.vim/plugged')

Plug 'vim-airline/vim-airline'

call plug#end()

" vim-airline
let g:airline#extensions#tabline#enabled = 1 " turn on buffer list
set laststatus=2 " turn on bottom bar
nmap <F3> :bp<CR>
nmap <F4> :bn<CR>

:PlugInstall
```
