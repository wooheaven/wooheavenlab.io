---
title: "Viewing the configuration file of Vim, which is typically named .vimrc"
date: 2023-06-08T23:13:55+09:00
draft: false
toc: false
images:
tags:
  - Ubuntu20
  - vim
  - .vimrc
categories:
  - Posts
  - Linux
  - Ubuntu
---

## View .vimrc 
```
$ cat ~/.vimrc
" BackSpace Key
set backspace=indent,eol,start

" git commit
nnoremap @c <Esc>:5,5s/; On branch /#/<CR>o```<Esc>:10,$s/;\t//<CR><Esc>:$,$s/;/```/<CR><Esc>

" View White Characters or not
set listchars=eol:$,tab:<->,space:.,trail:~,extends:>,precedes:<
let @l=':set list! list?'

" vim diff refresh
set autoread
nmap <F5> <ESC>:checktime<CR>

" tab to 4 spaces
set ts=4
let @t=':s/\t/    /g'

" Line Number
let @n=':set nu! nu?'
set nu

" encoding
set encoding=utf8

".vim-plug
call plug#begin('~/.vim/plugged')

Plug 'nanotech/jellybeans.vim'
Plug 'vim-airline/vim-airline'
Plug 'scrooloose/nerdtree'

call plug#end()

" vim-airline
let g:airline#extensions#tabline#enabled = 1 " turn on buffer list
set laststatus=2 " turn on bottom bar
nmap <F3> :bp<CR>
nmap <F4> :bn<CR>

" NERD tree shortcut
nmap <F2> :NERDTreeToggle<CR>

" Colors
set background=dark
colorscheme jellybeans
```
