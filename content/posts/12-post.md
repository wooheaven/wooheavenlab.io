---
title: "Install Vim plugin nerdtree using vim-plug"
date: 2023-05-26T14:36:24+09:00
draft: false
toc: false
images:
tags:
  - Ubuntu20
  - vim
  - vim_plugin
  - vim-plug
  - nerdtree
  - .vimrc
categories:
  - Posts
  - Linux
  - Ubuntu
---

##  Configure ~/.vimrc
```
$ vi ~/.vimrc
call plug#begin('~/.vim/plugged')

Plug 'scrooloose/nerdtree'

call plug#end()

" NERD tree shortcut
nmap <F2> :NERDTreeToggle<CR>

:PlugInstall
```
