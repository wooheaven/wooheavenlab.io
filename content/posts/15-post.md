---
title: "Convert an audio file WAV from stereo to mono using ffmpeg"
date: 2023-05-26T23:45:38+09:00
draft: false
toc: false
images:
tags:
  - Ubuntu20
  - Audio
  - WAV
  - ffmpeg
categories:
  - Posts
  - Linux
  - Ubuntu
---

## Before convert
```
$ file coughvid_20211012/002877709-f7ea-4789-9526-1e1f47cafb3f.wav
coughvid_20211012/002877709-f7ea-4789-9526-1e1f47cafb3f.wav: RIFF (little-endian) data, WAVE audio, Microsoft PCM, 16 bit, stereo 48000 Hz
```

## Convert an audio file WAV from stereo to mono using ffmpeg
```
$ ffmpeg -i coughvid_20211012/02877709-f7ea-4789-9526-1e1f47cafb3f.wav -ac 1 02877709-f7ea-4789-9526-1e1f47cafb3f.wav
ffmpeg version 6.0-essentials_build-www.gyan.dev Copyright (c) 2000-2023 the FFmpeg developers
  built with gcc 12.2.0 (Rev10, Built by MSYS2 project)
  configuration: --enable-gpl --enable-version3 --enable-static --disable-w32threads --disable-autodetect --enable-fontconfig --enable-iconv --enable-gnutls --enable-libxml2 --enable-gmp --enable-lzma --enable
-zlib --enable-libsrt --enable-libssh --enable-libzmq --enable-avisynth --enable-sdl2 --enable-libwebp --enable-libx264 --enable-libx265 --enable-libxvid --enable-libaom --enable-libopenjpeg --enable-libvpx --
enable-libass --enable-libfreetype --enable-libfribidi --enable-libvidstab --enable-libvmaf --enable-libzimg --enable-amf --enable-cuda-llvm --enable-cuvid --enable-ffnvcodec --enable-nvdec --enable-nvenc --en
able-d3d11va --enable-dxva2 --enable-libmfx --enable-libgme --enable-libopenmpt --enable-libopencore-amrwb --enable-libmp3lame --enable-libtheora --enable-libvo-amrwbenc --enable-libgsm --enable-libopencore-am
rnb --enable-libopus --enable-libspeex --enable-libvorbis --enable-librubberband
  libavutil      58.  2.100 / 58.  2.100
  libavcodec     60.  3.100 / 60.  3.100
  libavformat    60.  3.100 / 60.  3.100
  libavdevice    60.  1.100 / 60.  1.100
  libavfilter     9.  3.100 /  9.  3.100
  libswscale      7.  1.100 /  7.  1.100
  libswresample   4. 10.100 /  4. 10.100
  libpostproc    57.  1.100 / 57.  1.100
Guessed Channel Layout for Input Stream #0.0 : stereo
Input #0, wav, from 'coughvid_20211012/02877709-f7ea-4789-9526-1e1f47cafb3f.wav':
  Metadata:
    encoder         : Lavf60.3.100
  Duration: 00:00:09.99, bitrate: 1536 kb/s
  Stream #0:0: Audio: pcm_s16le ([1][0][0][0] / 0x0001), 48000 Hz, 2 channels, s16, 1536 kb/s
Stream mapping:
  Stream #0:0 -> #0:0 (pcm_s16le (native) -> pcm_s16le (native))
Press [q] to stop, [?] for help
Output #0, wav, to '02877709-f7ea-4789-9526-1e1f47cafb3f.wav':
  Metadata:
    ISFT            : Lavf60.3.100
  Stream #0:0: Audio: pcm_s16le ([1][0][0][0] / 0x0001), 48000 Hz, mono, s16, 768 kb/s
    Metadata:
      encoder         : Lavc60.3.100 pcm_s16le
size=     937kB time=00:00:09.98 bitrate= 768.8kbits/s speed= 387x
video:0kB audio:937kB subtitle:0kB other streams:0kB global headers:0kB muxing overhead: 0.008130%
```

## After convert
```
$ file coughvid_20211012/02877709-f7ea-4789-9526-1e1f47cafb3f.wav
coughvid_20211012/02877709-f7ea-4789-9526-1e1f47cafb3f.wav: RIFF (little-endian) data, WAVE audio, Microsoft PCM, 16 bit, mono 48000 Hz
```

## ffmpeg reference
[https://trac.ffmpeg.org/wiki/AudioChannelManipulation#stereomonostream](https://trac.ffmpeg.org/wiki/AudioChannelManipulation#stereomonostream)
