---
title: "Install vim-plug for managing plugins of Vim"
date: 2023-05-22T00:36:00+09:00
draft: false
toc: false
images:
tags:
  - Ubuntu20
  - vim
  - vim_plugin
  - vim-plug
  - .vimrc
categories:
  - Posts
  - Linux
  - Ubuntu
---

## Install vim-plug
```
$ curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```

## configure vim-plug
```
$ vi ~/.vimrc
".vim-plug
call plug#begin('~/.vim/plugged')

call plug#end()
```
