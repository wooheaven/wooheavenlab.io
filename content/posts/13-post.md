---
title: "Install Vim plugin jellybeans.vim using vim-plug"
date: 2023-05-26T22:19:07+09:00
draft: false
toc: false
images:
tags:
  - Ubuntu20
  - vim
  - vim_plugin
  - vim-plug
  - jellybeans.vim 
  - .vimrc
categories:
  - Posts
  - Linux
  - Ubuntu
---

##  Configure ~/.vimrc
```
$ vi ~/.vimrc
" vim-plug
call plug#begin('~/.vim/plugged')

Plug 'nanotech/jellybeans.vim'

call plug#end()

" Colors
colorscheme jellybeans

:PlugInstall
```
